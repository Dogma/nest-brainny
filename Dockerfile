FROM node

WORKDIR /home/api

COPY package.json .
COPY yarn.lock .
COPY . .

RUN yarn

CMD yarn start:dev