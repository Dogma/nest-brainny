import { InputType } from "@nestjs/graphql";
import { IsEmail, IsNotEmpty, IsString } from "class-validator";

@InputType()
export class CreateUserInput {
  @IsString()
  @IsNotEmpty({message: 'Este campo não estar vazio'})
  name: string;

  @IsEmail()
  @IsNotEmpty({message: 'Este campo não estar vazio'})
  email: string;
}