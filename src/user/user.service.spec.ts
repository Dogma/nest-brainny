import { InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import TestUtil from '../common/test/TestUtil';
import { User } from './user.entity';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  const mockRepository = {
    find: jest.fn(),
    findOne: jest.fn(),
    create: jest.fn(),
    save: jest.fn(),
    update: jest.fn(),
    delete: jest.fn(),
  }

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserService, {
        provide: getRepositoryToken(User),
        useValue: mockRepository,
      }],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  beforeEach(() => {
    mockRepository.create.mockReset();
    mockRepository.delete.mockReset();
    mockRepository.find.mockReset();
    mockRepository.findOne.mockReset();
    mockRepository.save.mockReset();
    mockRepository.update.mockReset();
  })

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAllUsers', () => {
    it('should be list all users', async () => {
      const user = TestUtil.giveMeValidUser();
      mockRepository.find.mockReturnValue([user, user]);
      const users = await service.findAllUsers();
      expect(users).toEqual([user, user]);
    })
  })

  describe('findUserById', () => {
    it('should find a user', async () => {
      const user = TestUtil.giveMeValidUser();
      mockRepository.findOne.mockReturnValue(user);
      const userFound = await service.findUserById('1');
      expect(userFound).toMatchObject({name: user.name});
      expect(mockRepository.findOne).toHaveBeenCalledTimes(1);
    })
    it('should return a exception when does not to find a user', async () => {
      mockRepository.findOne.mockReturnValue(null);
      expect(service.findUserById('invalidId')).rejects.toBeInstanceOf(NotFoundException);
    })
  })

  describe('create user', () => {
    it('should create a user', async () => {
      const user = TestUtil.giveMeValidUser();
      mockRepository.save.mockReturnValue(user);
      mockRepository.create.mockReturnValue(user);
      const savedUser = await service.createUser(user);
      expect(savedUser).toMatchObject(user);
      expect(mockRepository.create).toBeCalledTimes(1);
      expect(mockRepository.save).toBeCalledTimes(1);
    })

    it('should return a exception when doesnt create a user', async () => {
      const user = TestUtil.giveMeValidUser();
      mockRepository.save.mockReturnValue(null);
      mockRepository.create.mockReturnValue(user);
      await service.createUser(user).catch(e => {
        expect(e).toBeInstanceOf(InternalServerErrorException);
        expect(e).toMatchObject({message: 'Problema para criar um usuário'})
      });
      expect(mockRepository.create).toBeCalledTimes(1);
      expect(mockRepository.save).toBeCalledTimes(1);
    })
  })

  describe('updateUser', () => {
    it('should update a user', async () => {
      const user = TestUtil.giveMeValidUser();
      const updateUser = {name: 'updateName'}
      mockRepository.findOne.mockReturnValue(user);
      mockRepository.update.mockReturnValue({
        ...user,
        name: 'updateName',
      });
      mockRepository.create.mockReturnValue({
        ...user, ...updateUser,
      });
      const resultUser = await service.updateUser('1', user);
      expect(resultUser).toMatchObject({...user, ...updateUser});
      expect(mockRepository.findOne).toBeCalledTimes(1);
      expect(mockRepository.create).toBeCalledTimes(1);
      expect(mockRepository.update).toBeCalledTimes(1);
    })
    
    describe('deleteUser', () => {
      it('should delete a esisting user', async () => {
        const user = TestUtil.giveMeValidUser();
        mockRepository.delete.mockReturnValue(user);
        mockRepository.findOne.mockReturnValue(user);
        const deleteUser = await service.deleteUser('1');
        expect(deleteUser).toBe(true);
        expect(mockRepository.findOne).toBeCalledTimes(1);
        expect(mockRepository.delete).toBeCalledTimes(1);
      })

      it('should not delete a inexisting user', async () => {
        const user = TestUtil.giveMeValidUser();
        mockRepository.delete.mockReturnValue(null);
        mockRepository.findOne.mockReturnValue(user);
        const deleteUser = await service.deleteUser('1');
        expect(deleteUser).toBe(false);
        expect(mockRepository.findOne).toBeCalledTimes(1);
        expect(mockRepository.delete).toBeCalledTimes(1);
      })
    })
  })
});
