import { User } from "./../../user/user.entity";

export default class TestUtil {
  static giveMeValidUser(): User {
    const user = new User();
    user.name = 'Valid Name';
    user.email = 'Valid@Email.com';
    return user;
  }
}